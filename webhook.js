import {failure, success} from "./libs/response-lib";
var AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1'});

var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

export function receiveWebhook(event, context, callback){
  const id = event.pathParameters.id;
  const message = event.body;
  console.log('ID::: ', id);
  console.log('MSG::: ', message);

var params = {
  DelaySeconds: 10,
  MessageAttributes:{
    'OrgId': {
      DataType: 'String',
      StringValue: id
    },
    'Webhook': {
      DataType: 'String',
      StringValue: message
    }
  },
  MessageBody: 'Its a webhook',
  QueueUrl: 'https://sqs.eu-west-1.amazonaws.com/448241683921/TEST_QUEUE'
};

sqs.sendMessage(params, function(err, data) {
  if(err){
    console.log('Error : ', err);
    callback(null, failure({ status: false }));
  } else {
    console.log('Success : ', data.MessageId);
    callback(null, success(data));
  }
});
}