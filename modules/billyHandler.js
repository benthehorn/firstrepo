import axios  from 'axios';
import * as _ from 'lodash';

export async function getInvoices(client) {
  const res = await client.request('GET', '/invoices');
  return res.invoices;
}

export async function getInvoice(client, id) {
  const res = await client.request('GET', `/invoices/${id}`);
  return res.invoice;
}

// Creates an invoice, the server replies with a list of invoices and we
// return the id of the first invoice of the list
export async function createInvoice(client, organizationId, reepayInvoice, customerId, items, taxIncluded) {
  const invoice = {
    organizationId: organizationId,
    invoiceNo: 'reepay_' + reepayInvoice.id,
    entryDate: new Date().toISOString().slice(0, 10),
    dueDate: reepayInvoice.due.slice(0, 10),
    contactId: customerId,
    lines: items,
    state: 'approved',
    taxMode: taxIncluded
  };
  const res = await client.request('POST', '/invoices', { invoice: invoice });
  return res.invoices[0];
}

export async function getOrganization(client) {
  const res = await client.request('GET', '/organization');
  return res.organization;
}

export async function makeBankPayment(client, invoice, cashAccountId) {
  const res = await client.request('POST', '/bankPayments', {
    bankPayment: {
      cashAmount: invoice.balance.toFixed(2),
      entryDate: invoice.entryDate,
      cashSide: 'debit',
      cashAccountId: cashAccountId,
      associations: [
        {
          subjectReference: `invoice:${invoice.id}`
        }
      ]
    }
  });
  return res.meta;
}

export async function createContact(client, organizationId, customer) {
  const customerType = customer.vat ? 'company' : 'person';
  let contact = {
    organizationId: organizationId,
    name: `${customer.first_name} ${customer.last_name}`,
    street: customer.address,
    zipcodeText: customer.postal_code,
    cityText: customer.city,
    countryId: customer.country,
    type: customerType,
    phone: customer.phone,
    contactNo: 'reepay_' + customer.handle
  };
  if (customer.vat) contact.registrationNo = customer.vat;
  const res = await client.request('POST', '/contacts', { contact: contact });
  return res.contacts[0];
}

export async function getContact(client, organizationId, contactId) {
  const res = await client.request('GET', `/contacts/${contactId}`);
  return res.contact;
}

export async function getContacts(client) {
  const res = await client.request('GET', '/contacts');
  return res.contacts;
}

export async function createProduct(client, billyOrgId, product, taxRule, line) {

  if (_.startsWith(product.handle, 'ondemand')) { // TODO: Fix the missing description on the first ONDEMAND
    product.description = '';
  }

  if (_.startsWith(product.handle, 'dis')) {
    product.handle += '-' + line.vat;
  }

  const newProduct = {
    organizationId: billyOrgId,
    name: product.name,
    description: product.description,
    productNo: 'reepay_' + product.handle,
    salesTaxRulesetId: taxRule.RuleSetId
  };
  const res = await client.request('POST', '/products', { product: newProduct });
  return res.products[0];
}

export async function getProducts(client) {
  const res = await client.request('GET', '/products');
  return res.products;
}

export async function getSalesTaxRulesets(client) {
  const res = await client.request('GET', '/salesTaxRulesets');
  return res.salesTaxRulesets;
}

export async function createSalesTaxRuleset(client, ruleSet) {
  const res = await client.request('POST', '/salesTaxRulesets', { salesTaxRuleset: ruleSet });
  return res.salesTaxRulesets[0];
}

export async function getTaxRates(client) {
  const res = await client.request('GET', '/taxRates');
  return res.taxRates;
}

export async function createTaxRate(client, taxRate) {
  const res = await client.request('POST', '/taxRates', { taxRate: taxRate });
  return res.taxRates[0];
}
