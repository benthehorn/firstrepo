import axios  from 'axios';
import base64 from 'base-64';
import * as _ from 'lodash';

const apiBaseUrl = 'https://api.reepay.com/v1/';
let allWebhooks = [];
export async function getInvoice(token) {
  const authToken = 'Basic ' + base64.encode(token + ':');
  const url = `${apiBaseUrl}invoice`;
  try {
    const res = await axios.get(url, { headers: { Authorization: authToken } });
    return res.data;
  } catch (error) { // TODO: Consider how to handle a network down error
    return { status: error.response.status, text: error.response.statusText };
  }
}

export async function getCustomer(token, customerId) {
  const authToken = 'Basic ' + base64.encode(token + ':');
  const url = `${apiBaseUrl}customer/${customerId}`;
  try {
    const res = await axios.get(url, { headers: { Authorization: authToken } });
    return res.data;
  } catch (error) { // TODO: Consider how to handle a network down error
    return { status: error.response.status, text: error.response.statusText };
  }
}

export async function getProduct(token, orderline) {
  const authToken = 'Basic ' + base64.encode(token + ':');
  const url = `${apiBaseUrl}${orderline.origin}/${orderline.origin_handle}`;
  try {
    const res = await axios.get(url, { headers: { Authorization: authToken } });
    return res.data;
  } catch (error) { // TODO: Consider how to handle a network down error
    return { status: error.response.status, text: error.response.statusText };
  }
}

export async function findDiscount(token, subscriptionHandle, orderline) {
  const authToken = 'Basic ' + base64.encode(token + ':');
  const url = `${apiBaseUrl}subscription/${subscriptionHandle}/discount/${orderline.origin_handle}`;
  try {
    const res = await axios.get(url, { headers: { Authorization: authToken } });
    return res.data;
  } catch (error) { // TODO: Consider how to handle a network down error
    return { status: error.response.status, text: error.response.statusText };
  }
}

export async function getAllWebhooks(token, date) {
  const token1 = token;
  let webhooks = [];
  const authToken = 'Basic ' + base64.encode(token + ':');
  const url = `${apiBaseUrl}webhook?size=100&created_before=${date}`;
  try {
    const res = await axios.get(url, { headers: { Authorization: authToken } });
    res.data.forEach(hook => {
      allWebhooks.push(hook);
    });
    const last = allWebhooks[allWebhooks.length - 1];
    const lastDate = new Date(last.created);
    if (res.data.length > 0) {
      await getAllWebhooks(token1, lastDate.toISOString());
    } else {
      console.log('lasthook :', allWebhooks.length);
    }

  } catch (error) { // TODO: Consider how to handle a network down error
    return { status: error.response.status, text: error.response.statusText };
  }

  return allWebhooks;
  allWebhooks = [];
}

