import * as RH            from './modules/reepayHandler';
import {failure, success} from "./libs/response-lib";

export async function getInvoices(event, context, callback){
  const apiKey = event.pathParameters.apiKey;
const reepayKey = 'priv_f6a62053a621db0189bc26839decc279';
  try {
    const result = await RH.getInvoice(apiKey);
    if (result) {
      // Return the retrieved organization
      callback(null, success(result));
    } else {
      callback(null, failure({ status: false, error: "Item not found." }));
    }
  } catch (e) {
    callback(null, failure({ status: false }));
  }
}