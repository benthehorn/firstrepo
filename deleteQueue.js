var AWS = require('aws-sdk');
//change this to eu-west-1
AWS.config.update({ region: 'eu-west-1'});

var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var params = {
  QueueUrl: 'QUEUE_URL'
};

sqs.deleteQueue(params, function(err, data){
  if(err){
    console.log('Error : ', err);
  } else {
    console.log('Success : ', data);
  }
});
