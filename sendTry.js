var AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1'});

var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var params = {
  DelaySeconds: 10,
  MessageAttributes:{
    'Title': {
      DataType: 'String',
      StringValue: 'Ben first message'
    },
    'Author': {
      DataType: 'String',
      StringValue: 'Ben'
    }
  },
  MessageBody: 'Information about Bens new book',
  QueueUrl: 'https://sqs.eu-west-1.amazonaws.com/448241683921/TEST_QUEUE'
};

sqs.sendMessage(params, function(err, data){
  if(err){
    console.log('Error : ', err);
  } else {
    console.log('Success : ', data.MessageId);
  }
})






