import * as BH            from './modules/billyHandler';
import BC                 from './modules/BillyClient';
import {failure, success} from "./libs/response-lib";

export async function getOrg(event, context, callback){
  const orgId = event.orgId;
  const client = new BC(orgId);

  try {
  const result = await BH.getOrganization(client);
    if (result) {
      // Return the retrieved organization
      callback(null, success(result));
    } else {
      callback(null, failure({ status: false, error: "Item not found." }));
    }
  } catch (e) {
    callback(null, failure({ status: false }));
  }
}