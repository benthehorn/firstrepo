var AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1'});

var sqs = new AWS.SQS({ apiVersion: '2012-11-05'});

var params = {
  Attributes: {
    "RedrivePolicy": "{\"deadLetterTargetArn\":\"arn:aws:sqs:eu-west-1:448241683921:TEST_DEAD_LETTER_QUEUE\",\"maxReceiveCount\":\"10\"}",
  },
  QueueUrl: 'https://sqs.eu-west-1.amazonaws.com/448241683921/TEST_QUEUE'
};

sqs.setQueueAttributes(params, function(err, data) {
  if(err){
    console.log('Error : ', err);
  } else {
    console.log('Success : ', data);
  }
});


/*
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region
AWS.config.update({region: 'REGION'});

// Create the SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var params = {
  Attributes: {
    "RedrivePolicy": "{\"deadLetterTargetArn\":\"DEAD_LETTER_QUEUE_ARN\",\"maxReceiveCount\":\"10\"}",
  },
  QueueUrl: "SOURCE_QUEUE_URL"
};

sqs.setQueueAttributes(params, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data);
  }
});*/
